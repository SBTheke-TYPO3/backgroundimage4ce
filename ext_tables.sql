#
# Table structure for table "tt_content"
#
CREATE TABLE tt_content (
    tx_backgroundimage4ce_active tinyint(1) DEFAULT '0' NOT NULL,
    tx_backgroundimage4ce_image int(11) unsigned DEFAULT '0' NOT NULL,
    tx_backgroundimage4ce_repeat varchar(10) DEFAULT '' NOT NULL,
    tx_backgroundimage4ce_color varchar(20) DEFAULT '' NOT NULL,
    tx_backgroundimage4ce_position varchar(20) DEFAULT '' NOT NULL,
    tx_backgroundimage4ce_size varchar(20) DEFAULT '' NOT NULL,
    tx_backgroundimage4ce_attachment varchar(10) DEFAULT '' NOT NULL,
    tx_backgroundimage4ce_autoheight tinyint(1) DEFAULT '0' NOT NULL,
    tx_backgroundimage4ce_height int(1) DEFAULT '0' NOT NULL,
    tx_backgroundimage4ce_opacity int(1) DEFAULT '0' NOT NULL
);
