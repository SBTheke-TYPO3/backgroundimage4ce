<?php
defined('TYPO3') || die();

// Add static template
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('backgroundimage4ce', 'Configuration/TypoScript/', 'Background image for content elements');
