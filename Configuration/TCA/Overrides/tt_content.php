<?php
defined('TYPO3') || die();

$ll = 'LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:';

(function($table, $ll) {
    // Extra fields for the tt_content table
    $newContentColumns = [
        'tx_backgroundimage4ce_active' => [
            'exclude' => 1,
            'label' => $ll . 'tt_content.tx_backgroundimage4ce_active',
            'onChange' => 'reload',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
            ],
        ],
        'tx_backgroundimage4ce_image' => [
            'exclude' => 1,
            'label' => $ll . 'tt_content.tx_backgroundimage4ce_image',
            'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
            'config' => [
                'type' => 'file',
                'foreign_table' => 'sys_file_reference',
                'foreign_field' => 'uid_foreign',
                'foreign_sortby' => 'sorting_foreign',
                'foreign_table_field' => 'tablenames',
                'foreign_match_fields' => [
                    'fieldname' => 'tx_backgroundimage4ce_image',
                ],
                'appearance' => [
                    'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                ],
                'maxitems' => 1,
                'overrideChildTca' => [
                    'types' => [
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_UNKNOWN => [
                            'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                    ],
                ],
            ],
        ],
        'tx_backgroundimage4ce_repeat' => [
            'exclude' => 1,
            'label' => $ll . 'tt_content.tx_backgroundimage4ce_repeat',
            'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        $ll . 'tt_content.tx_backgroundimage4ce_default',
                        ''
                    ],
                    [
                        $ll . 'tt_content.tx_backgroundimage4ce_repeat.repeat',
                        'repeat'
                    ],
                    [
                        $ll . 'tt_content.tx_backgroundimage4ce_repeat.norepeat',
                        'no-repeat'
                    ],
                    [
                        $ll . 'tt_content.tx_backgroundimage4ce_repeat.repeatx',
                        'repeat-x'
                    ],
                    [
                        $ll . 'tt_content.tx_backgroundimage4ce_repeat.repeaty',
                        'repeat-y'
                    ],
                ],
            ],
        ],
        'tx_backgroundimage4ce_color' => [
            'exclude' => 1,
            'label' => $ll . 'tt_content.tx_backgroundimage4ce_color',
            'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
            'config' => [
                'type' => 'input',
                'renderType' => 'colorpicker',
                'max' => 20,
                'eval' => 'trim',
            ],
        ],
        'tx_backgroundimage4ce_position' => [
            'exclude' => 1,
            'label' => $ll . 'tt_content.tx_backgroundimage4ce_position',
            'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
            'config' => [
                'type' => 'input',
                'max' => 20,
                'eval' => 'trim',
            ],
        ],
        'tx_backgroundimage4ce_size' => [
            'exclude' => 1,
            'label' => $ll . 'tt_content.tx_backgroundimage4ce_size',
            'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
            'config' => [
                'type' => 'input',
                'max' => 20,
                'eval' => 'trim',
            ],
        ],
        'tx_backgroundimage4ce_attachment' => [
            'exclude' => 1,
            'label' => $ll . 'tt_content.tx_backgroundimage4ce_attachment',
            'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        $ll . 'tt_content.tx_backgroundimage4ce_default',
                        ''
                    ],
                    [
                        $ll . 'tt_content.tx_backgroundimage4ce_attachment.scroll',
                        'scroll'
                    ],
                    [
                        $ll . 'tt_content.tx_backgroundimage4ce_attachment.fixed',
                        'fixed'
                    ],
                    [
                        $ll . 'tt_content.tx_backgroundimage4ce_attachment.local',
                        'local'
                    ],
                ],
            ],
        ],
        'tx_backgroundimage4ce_autoheight' => [
            'exclude' => 1,
            'label' => $ll . 'tt_content.tx_backgroundimage4ce_autoheight',
            'onChange' => 'reload',
            'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
            'config' => [
                'type' => 'check',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_core.xlf:labels.enabled',
                        ''
                    ]
                ],
            ],
        ],
        'tx_backgroundimage4ce_height' => [
            'exclude' => 1,
            'label' => $ll . 'tt_content.tx_backgroundimage4ce_height',
            'displayCond' => ['AND' => ['FIELD:tx_backgroundimage4ce_active:REQ:true', 'FIELD:tx_backgroundimage4ce_autoheight:=:0']],
            'config' => [
                'type' => 'input',
                'size' => 5,
                'max' => 5,
                'eval' => 'int',
            ],
        ],
        'tx_backgroundimage4ce_opacity' => [
            'exclude' => 1,
            'label' => $ll . 'tt_content.tx_backgroundimage4ce_opacity',
            'displayCond' => 'FIELD:tx_backgroundimage4ce_active:REQ:true',
            'config' => [
                'type' => 'input',
                'range' => [
                    'lower' => 0,
                    'upper' => 100
                ],
                'slider' => [
                    'step' => 1,
                    'width' => 200
                ],
                'eval' => 'int'
            ],
        ],
    ];

    // Adding fields to the tt_content table definition in TCA
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns($table, $newContentColumns);

    // Create palette
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes($table, '--palette--;LLL:EXT:backgroundimage4ce/Resources/Private/Language/locallang.xlf:tt_content.palette.backgroundimage4ce;backgroundimage4ce', '', 'after:layout');
    $GLOBALS['TCA'][$table]['palettes']['backgroundimage4ce']['showitem'] = 'tx_backgroundimage4ce_active, --linebreak--, tx_backgroundimage4ce_image, --linebreak--, tx_backgroundimage4ce_repeat, tx_backgroundimage4ce_color, tx_backgroundimage4ce_position, tx_backgroundimage4ce_size, --linebreak--, tx_backgroundimage4ce_opacity, tx_backgroundimage4ce_attachment, tx_backgroundimage4ce_autoheight, tx_backgroundimage4ce_height';

})('tt_content', $ll);
