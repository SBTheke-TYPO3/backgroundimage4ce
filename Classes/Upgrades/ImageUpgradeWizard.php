<?php

namespace SBTheke\Backgroundimage4ce\Upgrades;

use Doctrine\DBAL\Exception;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

final class ImageUpgradeWizard implements UpgradeWizardInterface
{
    private const TABLE_NAME = 'sys_file_reference';
    private const TABLE_COLUMN = 'fieldname';
    private const VALUE_OLD = 'tx_backgroundimage4ce';
    private const VALUE_NEW = 'tx_backgroundimage4ce_image';

    /**
     * Return the speaking name of this wizard
     */
    public function getTitle(): string
    {
        return 'EXT:backgroundimage4ce: Update images';
    }

    /**
     * Return the description for this wizard
     */
    public function getDescription(): string
    {
        return sprintf(
            'Updates records in table "%s", if they use the previously used value "%s" for column "%s".',
            self::TABLE_NAME,
            self::VALUE_OLD,
            self::TABLE_COLUMN,
        );
    }

    /**
     * Execute the update
     *
     * Called when a wizard reports that an update is necessary
     */
    public function executeUpdate(): bool
    {
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable(self::TABLE_NAME);
        $queryBuilder = $connection->createQueryBuilder();

        $affectedRows = $queryBuilder
            ->update(self::TABLE_NAME)
            ->where(
                $queryBuilder->expr()->eq(
                    self::TABLE_COLUMN,
                    $queryBuilder->createNamedParameter(self::VALUE_OLD)
                )
            )
            ->set(self::TABLE_COLUMN, self::VALUE_NEW)
            ->executeStatement();

        return (bool)$affectedRows;
    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool Whether an update is required (TRUE) or not (FALSE)
     * @throws Exception
     */
    public function updateNecessary(): bool
    {
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable(self::TABLE_NAME);
        $queryBuilder = $connection->createQueryBuilder();
        $count = $queryBuilder
            ->count('uid')
            ->from(self::TABLE_NAME)
            ->where(
                $queryBuilder->expr()->eq(self::TABLE_COLUMN, $queryBuilder->createNamedParameter(self::VALUE_OLD))
            )
            ->executeQuery()
            ->fetchOne();
        return (bool)$count;
    }

    /**
     * Returns an array of class names of prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return string[]
     */
    public function getPrerequisites(): array
    {
        return [];
    }
}
