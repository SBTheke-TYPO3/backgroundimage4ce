<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 Sven Burkert <bedienung@sbtheke.de>, SBTheke web development
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SBTheke\Backgroundimage4ce\ViewHelpers;

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Page\AssetCollector;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class RenderBackgroundViewHelper extends AbstractViewHelper {

    public function initializeArguments(): void
    {
        $this->registerArgument('cObj', 'array', 'Content Object');
    }

    /**
     * @return string
     */
    public function render(): string
    {
        $cObj = $this->arguments['cObj'];
        if($cObj['tx_backgroundimage4ce_active']) {
            $setup = $this->getRequest()->getAttribute('frontend.typoscript')->getSetupArray();
            $configuration = $setup['plugin.']['tx_backgroundimage4ce.'];

            $declaration = [];

            // Generate class name
            if($configuration['classStdWrap.']) {
                $contentObjectRenderer = GeneralUtility::makeInstance(ContentObjectRenderer::class);
                $className = $contentObjectRenderer->stdWrap($cObj['uid'], $configuration['classStdWrap.']);
            } else {
                $className = sprintf('backgroundimage4ce-c%s', $cObj['uid']);
            }

            // Get (default) settings from TypoScript setup
            if(!empty($configuration['css.']['default.'] && is_array($configuration['css.']['default.']))) {
                $declaration = $configuration['css.']['default.'];
            }

            // Get settings from content object
            if($cObj['tx_backgroundimage4ce_repeat']) {
                $declaration['background-repeat'] = $cObj['tx_backgroundimage4ce_repeat'];
            }
            if($cObj['tx_backgroundimage4ce_color']) {
                $declaration['background-color'] = $cObj['tx_backgroundimage4ce_color'];
            }
            if($cObj['tx_backgroundimage4ce_position']) {
                $declaration['background-position'] = $cObj['tx_backgroundimage4ce_position'];
            }
            if($cObj['tx_backgroundimage4ce_size']) {
                $declaration['background-size'] = $cObj['tx_backgroundimage4ce_size'];
            }
            if($cObj['tx_backgroundimage4ce_attachment']) {
                $declaration['background-attachment'] = $cObj['tx_backgroundimage4ce_attachment'];
            }
            if($cObj['tx_backgroundimage4ce_opacity']) {
                $backgroundColorRgb = sscanf($cObj['tx_backgroundimage4ce_color'] ?: '#ffffff', "#%02x%02x%02x");
                if($cObj['tx_backgroundimage4ce_color']) {
                    $declaration['background-color'] = sprintf('rgba(%s,%.2f)', implode(',', $backgroundColorRgb), $cObj['tx_backgroundimage4ce_opacity'] / 100);
                }
            }

            // Process background image
            $uid = $cObj['uid']; // Content element uid
            $fileRepository = GeneralUtility::makeInstance(FileRepository::class);
            $fileReferences = $fileRepository->findByRelation('tt_content', 'tx_backgroundimage4ce_image', $uid);
            if(count($fileReferences)) {
                $fileReference = array_shift($fileReferences); // Get first image
                $fileObject = $fileReference->getOriginalFile();
                if($configuration['adaptiveImages'] && is_array($configuration['adaptiveImages.'])) {
                    $declarationMediaQueries = '';
                    foreach($configuration['adaptiveImages.'] as $v) {
                        [$processedFile, $processedFileHeight] = self::prepareImage($fileObject, $v['image']);
                        if($processedFile) {
                            // Calculate auto height
                            if($cObj['tx_backgroundimage4ce_autoheight'] && $processedFileHeight) {
                                $minHeight = $processedFileHeight;
                                if($configuration['padding']) {
                                    $minHeight -= (int)$configuration['padding'];
                                }
                            }
                            // Build media query
                            $declarationMediaQueries .= sprintf(
                                '@media %s {' . PHP_EOL . "\t" . '.%s {' . PHP_EOL . "\t" . "\t" . 'background-image: url("%s");%s%s' . PHP_EOL . "\t" . '}' . PHP_EOL . '}' . PHP_EOL,
                                $v['media'],
                                $className,
                                $processedFile,
                                $cObj['tx_backgroundimage4ce_opacity'] && isset($backgroundColorRgb)
                                    ? PHP_EOL . "\t" . "\t" . sprintf(
                                        'background-image: linear-gradient(to bottom, rgba(%1$s,%2$.2f) 0%%,rgba(%1$s,%2$.2f) 100%%),url("%3$s");',
                                        implode(',', $backgroundColorRgb),
                                        $cObj['tx_backgroundimage4ce_opacity'] / 100,
                                        $processedFile
                                    )
                                    : '',
                                isset($minHeight) ? PHP_EOL . "\t" . "\t" . sprintf('min-height: %dpx;', $minHeight) : ''
                            );
                        }
                    }
                } else {
                    [$processedFile, $processedFileHeight] = self::prepareImage($fileObject, $configuration['image.']['max']);
                    if($processedFile) {
                        // Calculate auto height
                        if($cObj['tx_backgroundimage4ce_autoheight'] && $processedFileHeight) {
                            $minHeight = $processedFileHeight;
                            if($configuration['padding']) {
                                $minHeight -= (int)$configuration['padding'];
                            }
                            $declaration['min-height'] = sprintf('%dpx', $minHeight);
                        }
                        $declaration['background-image'] = sprintf(
                            'url("%s")',
                            $processedFile
                        );
                    }
                }
            }

            // Calculate manual height
            if(!$cObj['tx_backgroundimage4ce_autoheight'] && $cObj['tx_backgroundimage4ce_height']) {
                $minHeight = (int)$cObj['tx_backgroundimage4ce_height'];
                if($configuration['padding']) {
                    $minHeight -= (int)$configuration['padding'];
                }
                $declaration['min-height'] = sprintf('%dpx', $minHeight);
            }

            // Build CSS code
            $cssStyles = '';
            foreach($declaration as $k => $v) {
                $cssStyles .= sprintf(
                    "\t" . '%s: %s;' . PHP_EOL,
                    $k,
                    $v
                );
            }
            $cssStyles = sprintf(
                '.%s {' . PHP_EOL . '%s}' . PHP_EOL,
                $className,
                $cssStyles
            );
            if(isset($declarationMediaQueries)) {
                $cssStyles .= $declarationMediaQueries;
            }

            GeneralUtility::makeInstance(AssetCollector::class)
                ->addStyleSheet(
                    'backgroundimage4ce-c' . $uid,
                    GeneralUtility::writeStyleSheetContentToTemporaryFile($cssStyles)
                );

            return $className;
        }
        return '';
    }

    /**
     * Convert an image to a specific width and return absolute file path to processed image
     *
     * @param File $fileObject
     * @param integer $width
     * @return array image path and image height
     */
    public static function prepareImage(File $fileObject, int $width): array
    {
        $processedFile = $fileObject->process(
            ProcessedFile::CONTEXT_IMAGECROPSCALEMASK,
            [
                'maxWidth' => $width,
            ]
        );
        return [PathUtility::getAbsoluteWebPath($processedFile->getPublicUrl()), (int)$processedFile->getProperty('height')];
    }

    private function getRequest(): ServerRequestInterface|null
    {
        if ($this->renderingContext->hasAttribute(ServerRequestInterface::class)) {
            return $this->renderingContext->getAttribute(ServerRequestInterface::class);
        }
        return null;
    }

}
