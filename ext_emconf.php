<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "backgroundimage4ce".
 *
 * Auto generated 29-12-2024 09:49
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Background for content elements',
  'description' => 'Allows background images and background options for all types of content elements (text, text with image, forms, plugins, ...). Background images are adaptive.',
  'category' => 'fe',
  'version' => '7.0.0',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => true,
  'author' => 'Sven Burkert',
  'author_email' => 'bedienung@sbtheke.de',
  'author_company' => 'SBTheke web development',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '13.4.0-13.4.99',
    ),
    'suggests' => 
    array (
      'fluid_styled_content' => '',
      'gridelements' => '',
    ),
    'conflicts' => 
    array (
    ),
  ),
);

